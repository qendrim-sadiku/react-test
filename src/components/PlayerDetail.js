import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const PlayerDetail = ({player}) => {

      return(
          <div className="player_detail">
              <div>
                  <Link to={'/'}><button className="btn list">List</button></Link>
              </div>
              <div>
                  <table className="detail_table">
                      <tbody>
                          <tr>
                            <td>Name</td>
                            <td>{player.name}</td>
                          </tr>
                          <tr>
                            <td>Country</td>
                            <td>{player.country}</td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      );
}

PlayerDetail.propTypes = {
    website: PropTypes.string,
    twitter: PropTypes.string,
};
export default PlayerDetail;
