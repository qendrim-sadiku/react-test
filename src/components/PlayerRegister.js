import React, { Component } from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import { connect } from 'react-redux';
import * as PlayerApi from '../api/PlayerApi';
import LoadingSpinner from './LoadingSpinner';
import axios from 'axios';


class PlayerRegister extends Component {
    constructor(props) {
        super(props);

        this.state = {
              name: '',
              country: '',
              alert: '',
              data:[],
              forms:[],
              loading: false, // will be true when ajax request is running
        }
      
        this.onChangeValue = this.onChangeValue.bind(this);
        this.onRegister = this.onRegister.bind(this);
        this.checkInputValues = this.checkInputValues.bind(this);
        this.hideAlert = this.hideAlert.bind(this);
        this.popAlertMessage = this.popAlertMessage.bind(this);
    }

    componentDidMount() {
        PlayerApi.getForms();  
    }
    registerPlayer(name,country) {
        return axios.post('http://localhost:3001/players', {
                        name: name,
                        country: country,
                    })
                    .then(response => {
                        PlayerApi.getPlayers();
                        this.setState({loading:false})
                        return response;
                    })
    }

    hideAlert() {
      this.setState({
        alert: null
      });
    }

    popAlertMessage(alertMessage){
        this.setState({ alert : <SweetAlert title= {alertMessage} onConfirm={this.hideAlert} /> });
    }

    onChangeValue(e) {
        let input = e.target.name;
        let value = e.target.value;

        if(input === 'name') {
            this.setState({ name: value }); 
        } else if(input === 'country') {
            this.setState({ country: value });
        } 
       
    }

    checkInputValues(inputName, inputValue, regExp) {
        let pattern = new RegExp(regExp);
        let alertMessage = "";
        if(inputValue === null || inputValue === "") {
            alertMessage = `Please fill out ${inputName}`;
            this.popAlertMessage(alertMessage);
            return false;
        } else if(!pattern.test(inputValue)) {
             if(inputName === 'country') {
                alertMessage = `You have entered invalid ${inputName}`;
            } else {
                alertMessage = `You have entered invalid ${inputName}.`;
            }

            this.popAlertMessage(alertMessage);
            return false;
        }

        return true;
    }

    onRegister(e) {
          if(this.checkInputValues("name", this.state.name, "^[A-Za-z ]{3,20}$") &&
            this.checkInputValues("country", this.state.country, "^[A-Za-z ]")
            ) {
               this.registerPlayer(this.state.name,  this.state.country);
               this.setState({loading:true})
          }
    }

    handleSubmit(e){
        e.preventDefault();
        e.target.reset();   
    }

    //  renderKeys () {
    //     const firstObject = Object.keys(this.props.data)[1];
        
              

    //     return Object.keys(this.props.data[firstObject])
        
    //       .map(value => {
    //         return <div>{value.options}</div>;
    //       });
    //   };

    render() {
        const { forms,data } = this.props;
        const { loading } = this.state;
        return(
            <div className = "data_register">
                <form onSubmit={this.handleSubmit}>
                    {forms.map(forms => {
                        if (forms.type === 'text') {
                            return (
                                <div>
                                    <label for="#form">{forms.label}</label>
                                    <input id="form" type={forms.type} min={forms.rules} name="name" placeholder={forms.placeholder} onChange={this.onChangeValue}/>
                                </div>
                            )
                        } else if (forms.type === 'select') {
                            return (
                                <div>
                                    <label for="#select" >{forms.label}</label>
                                        <select id="select" name={forms.name} type={forms.type} required onChange={this.onChangeValue}>
                                        <option value=""  selected>Choose your option</option>
                                        {forms.options.map(data => (
                                            <option value={data.label} key={data.label}>
                                                {data.value}
                                            </option>
                                        ))}
                                    </select>
                                </div>  
                            )
                        }
                    })} 
                    {loading ? <LoadingSpinner /> :  <button className = "btn register" onClick={this.onRegister}>REGISTER</button>  }
                
                    {this.state.alert}
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    players: state.players,
    forms:state.forms
});

export default connect(
    mapStateToProps
)(PlayerRegister);


