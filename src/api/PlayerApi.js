import axios from 'axios';
import store from '../store';
import { getPlayersSuccess, deletePlayerSuccess, getPlayerDetailSuccess, getFormSuccess } from '../actions';

export function getPlayers() {
    return axios.get('http://localhost:3001/players')
                .then(response => {
                    console.log('test');
                    store.dispatch(getPlayersSuccess(response.data));
                    return response;
                });
}

export function getForms() {
    return axios.get('http://localhost:3001/form_inputs')
    .then(response => {
        if (response.status === 200 && response != null) {
        store.dispatch(getFormSuccess(response.data));
        // console.log(response.data);
        return response.data;
        } else {
            console.log('problem');
          }
    });
}

export function deletePlayer(playerId) {
    return axios.delete('http://localhost:3001/players/' + playerId)
                .then(response => {
                    store.dispatch(deletePlayerSuccess(playerId));
                    return response;
                });
  }

export function getProfile(playerId) {
    return axios.get('http://localhost:3001/players/' + playerId)
                .then(response => {
                    store.dispatch(getPlayerDetailSuccess(response.data));
                    return response;
                })
}