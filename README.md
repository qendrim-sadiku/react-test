## What is it?

This is the Test for react 



## Libraries and tools
* create-react-app
* redux
* react-redux
* axios
* json-server
* lodash
* react-bootstrap-sweetalert


```

## Running
To start this application run command bellow
```
npm
```

To test your application,

```
you need to start both json sever and yarn start.
    
1. open 2 command prompts.
2. execute command bellow
    you need to go to src/data folder to find the db.json
   * json-server --watch db.json --port 3001
3. execute the below command on the other prompt.
   * yarn start
```

This is also watching for changes, so when you update some code, you don’t have to restart the server, it does that automatically
